package com.leeryu.collect.resository;

import com.leeryu.collect.entity.RelatedUser;
import com.leeryu.collect.entity.User;
import com.leeryu.collect.repository.RelatedUserRepository;
import com.leeryu.collect.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RelatedUserRepository relatedUserRepository;

    @Test
    void get_users() {
        final List<User> all = userRepository.findAll();
    }

    @Test
    @Transactional
    void get_relation_user() {
        final List<RelatedUser> users = relatedUserRepository.findAll();

        users.forEach(user -> {
            System.out.println(user.getEmail());
            System.out.println(user.getKeywords());
        });
    }
}
