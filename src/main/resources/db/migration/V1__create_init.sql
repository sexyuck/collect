create table user (
                                    email varchar(100) not null,
                                    password varchar(100) not null,
                                    primary key (email)
);

create table keyword (
                                        email varchar(100) not null,
                                        keyword_name varchar(45) not null,
                                        primary key (email, keyword_name)
);

create table site (
                                    email varchar(100) not null,
                                    url varchar(1000) not null,
                                    primary key (email, url)
);

create table result (
                                      result_seq bigint(20) not null auto_increment,
                                      email varchar(100) not null,
                                      keyword_name varchar(45) not null,
                                      title varchar(500) not null,
                                      url varchar(1000) not null,
                                      create_at datetime not null,
                                      primary key(result_seq)
);
