package com.leeryu.collect.batch;

import com.leeryu.collect.entity.RelatedKeyword;
import com.leeryu.collect.entity.RelatedSite;
import com.leeryu.collect.entity.RelatedUser;
import com.leeryu.collect.enums.NewsSiteTypeCode;
import com.leeryu.collect.repository.RelatedUserRepository;
import com.leeryu.collect.service.CrawlingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Configuration
public class CrawlingBatchJob {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    @Lazy
    private RelatedUserRepository relatedUserRepository;

    @Autowired
    private CrawlingService crawlingService;

    @Bean
    public Job crawlingJob() {
        return jobBuilderFactory.get("crawlingJob")
                .start(step())
                .build();
    }

    @Bean
    @JobScope
    public Step step() {
        return stepBuilderFactory
                .get("step")
                .<RelatedUser, RelatedUser>chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

    @Bean
    @JobScope
    public RepositoryItemReader<RelatedUser> reader() {
        Map<String, Sort.Direction> sorts = new HashMap<>();
        sorts.put("email", Sort.Direction.ASC);

        return new RepositoryItemReaderBuilder<RelatedUser>()
                .repository(relatedUserRepository)
                .methodName("findAll")
                .sorts(sorts)
                .maxItemCount(100)
                .name("relatedUserRepositoryReader")
                .build();
    }

    @Bean
    @JobScope
    public ItemProcessor<RelatedUser, RelatedUser> processor() {
        return user -> {
            log.info("processing user data..........{}", user);
            log.info("processing user sites..........{}", user.getSites());
            log.info("processing user keywords..........{}", user.getKeywords());

            return user;
        };
    }

    @Bean
    @JobScope
    public ItemWriter<RelatedUser> writer() {
        return (users) -> {
            for (RelatedUser user : users) {
                final List<RelatedSite> sites = user.getSites();
                final List<RelatedKeyword> keywords = user.getKeywords();
                for (RelatedSite site : sites) {
                    for (RelatedKeyword keyword : keywords) {
                        final NewsSiteTypeCode newsSiteTypeCode = NewsSiteTypeCode.findByName(site.getSiteId().getUrl());
                        if (newsSiteTypeCode != null) {
                            crawlingService.crawling(user.getEmail(), keyword.getKeywordId().getKeywordName(), newsSiteTypeCode);
                        }
                    }
                }
            }
        };
    }
}
