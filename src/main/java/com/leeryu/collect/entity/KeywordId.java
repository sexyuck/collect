package com.leeryu.collect.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class KeywordId implements Serializable {

    @Column(name = "email")
    private String email;
    @Column(name = "keyword_name")
    private String keywordName;
}
