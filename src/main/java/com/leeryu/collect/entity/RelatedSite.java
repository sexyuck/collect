package com.leeryu.collect.entity;

import lombok.*;

import javax.persistence.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "related_site")
@Table(name = "site")
public class RelatedSite {

    @EmbeddedId
    private SiteId siteId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "email", insertable = false, updatable = false)
    private RelatedUser user;
}
