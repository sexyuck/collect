package com.leeryu.collect.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "site")
public class Site {

    @EmbeddedId
    private SiteId siteId;

    public static Site createSite(String email, String url) {
        return new Site(SiteId.builder().email(email).url(url).build());
    }
}
