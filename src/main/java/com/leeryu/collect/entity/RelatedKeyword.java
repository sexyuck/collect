package com.leeryu.collect.entity;

import lombok.*;

import javax.persistence.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "related_keyword")
@Table(name = "keyword")
public class RelatedKeyword {

    @EmbeddedId
    private KeywordId keywordId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "email", insertable = false, updatable = false)
    private RelatedUser user;
}
