package com.leeryu.collect.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ToString
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "result")
public class Result extends AuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long resultSeq;
    private String email;
    private String keywordName;
    private String title;
    private String url;

    public Result(String email, String keywordName, String title, String url) {
        this.email = email;
        this.keywordName = keywordName;
        this.title = title;
        this.url = url;
    }
}
