package com.leeryu.collect.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "keyword")
public class Keyword {

    @EmbeddedId
    private KeywordId keywordId;

    public static Keyword createKeyword(String email, String keywordName) {
        return new Keyword(KeywordId.builder().email(email).keywordName(keywordName).build());
    }
}
