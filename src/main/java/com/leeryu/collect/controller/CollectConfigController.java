package com.leeryu.collect.controller;


import com.leeryu.collect.enums.NewsSiteTypeCode;
import com.leeryu.collect.service.KeywordService;
import com.leeryu.collect.service.SiteService;
import com.leeryu.collect.util.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@Controller
@RequestMapping("/news/collect")
public class CollectConfigController {

    private final KeywordService keywordService;
    private final SiteService siteService;

    @GetMapping("/config/site")
    public String siteConfig(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("newsSiteTypes", NewsSiteTypeCode.values());
        model.addAttribute("sites", siteService.getSitesByLoginUser(user.getUsername()));
        return "config/site";
    }

    @GetMapping("/config/keyword")
    public String keywordConfig(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("keywords", keywordService.getKeywordByLoginUser(user.getUsername()));
        return "config/keyword";
    }

    @ResponseBody
    @PostMapping("/config/keyword")
    public ResponseEntity<String> registerKeyword(@AuthenticationPrincipal User user, @RequestBody String keyword) {
        if (!StringUtils.hasText(keyword)) {
            throw new IllegalArgumentException("Invalid parameter");
        }
        keywordService.register(user.getUsername(), keyword);
        return ResponseEntity.ok(ResponseResult.SUCCESS);
    }

    @ResponseBody
    @PostMapping("/config/site")
    public ResponseEntity<String> registerSites(@AuthenticationPrincipal User user, @RequestBody String site) {
        if (!StringUtils.hasText(site)) {
            throw new IllegalArgumentException("Invalid parameter");
        }
        siteService.register(user.getUsername(), site);
        return ResponseEntity.ok(ResponseResult.SUCCESS);
    }

    @ResponseBody
    @DeleteMapping("/config/keyword/{keyword}")
    public ResponseEntity<String> deleteKeyword(@AuthenticationPrincipal User user, @PathVariable String keyword) {
        keywordService.delete(user.getUsername(), keyword);
        return ResponseEntity.ok(ResponseResult.SUCCESS);
    }

    @ResponseBody
    @DeleteMapping("/config/site/{url}")
    public ResponseEntity<String> deleteSite(@AuthenticationPrincipal User user, @PathVariable String url) {
        siteService.delete(user.getUsername(), url);
        return ResponseEntity.ok(ResponseResult.SUCCESS);
    }
}
