package com.leeryu.collect.controller;

import com.leeryu.collect.domain.response.ResultResponse;
import com.leeryu.collect.repository.ResultRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RequestMapping("/news/collect")
public class CollectResultController {

    private final ResultRepository repository;

    @GetMapping("/result")
    public String myNewsData(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("results", repository.findAllByEmailOrderByCreateAtDesc(user.getUsername())
                .stream().map(ResultResponse::new).collect(Collectors.toList()));
        return "result";
    }
}
