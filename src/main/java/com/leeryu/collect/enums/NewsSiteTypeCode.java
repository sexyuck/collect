package com.leeryu.collect.enums;

import com.leeryu.collect.domain.CrawlingResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public enum NewsSiteTypeCode {
    NAVER("https://search.naver.com/search.naver?where=news&sm=tab_opt&sort=1&photo=0&field=0&pd=7&ds=&de=&docid=&related=0&mynews=0" +
            "&office_type=0&office_section_code=0&news_office_checked=&nso=so%3Add%2Cp%3Aall&is_sug_officeid=0&query={query}"
            , document -> {
        final Elements select = document.getElementsByClass("news_area");

        return select.stream()
                .map(element -> {
                    final Elements aTag = element.getElementsByClass("news_tit");
                    return CrawlingResponse.builder().title(aTag.text()).url(aTag.attr("href")).build();
                })
                .collect(Collectors.toList());
    }),
    DAUM("https://search.daum.net/search?w=news&DA=STC&enc=utf8&cluster=y&cluster_page=1&sort=recency&p=1&period=u&sd=20220226000000&ed=20220227235959" +
            "&q={query}", document -> {
        final Elements select = document.getElementsByClass("wrap_cont");

        return select.stream()
                .map(element -> {
                    final Element aTag = element.children().get(0);
                    return CrawlingResponse.builder().title(aTag.text()).url(aTag.attr("href")).build();
                })
                .collect(Collectors.toList());
    });

    private final String url;
    private final Function<Document, List<CrawlingResponse>> function;

    public static NewsSiteTypeCode findByName(String name) {
        return Arrays.stream(values())
                .filter(e -> e.name().equals(name))
                .findAny()
                .orElse(null);
    }
}
