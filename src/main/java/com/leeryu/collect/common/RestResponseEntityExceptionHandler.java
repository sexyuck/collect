package com.leeryu.collect.common;

import com.leeryu.collect.util.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({DuplicateException.class})
    public ResponseEntity<String> handleDuplicateException(Exception e) {
        log.error("duplicate exception error : {}", e.getMessage());
        return new ResponseEntity<>(ResponseResult.FAIL, HttpStatus.BAD_REQUEST);
    }
}
