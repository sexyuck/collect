package com.leeryu.collect.domain.response;

import com.leeryu.collect.entity.Keyword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KeywordResponse {

    private String email;
    private String keywordName;

    public KeywordResponse(Keyword keyword) {
        this.email = keyword.getKeywordId().getEmail();
        this.keywordName = keyword.getKeywordId().getKeywordName();
    }
}
