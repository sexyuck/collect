package com.leeryu.collect.domain.response;

import com.leeryu.collect.entity.Site;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class SiteResponse {
    private String email;
    private String url;

    public SiteResponse(Site site) {
        this.email = site.getSiteId().getEmail();
        this.url = site.getSiteId().getUrl();
    }
}
