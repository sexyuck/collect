package com.leeryu.collect.domain.response;

import com.leeryu.collect.entity.Result;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResultResponse {
    private String keywordName;
    private String title;
    private String url;
    private LocalDateTime createAt;

    public ResultResponse(Result entity) {
        this.keywordName = entity.getKeywordName();
        this.title = entity.getTitle();
        this.url = entity.getUrl();
        this.createAt = entity.getCreateAt();
    }
}
