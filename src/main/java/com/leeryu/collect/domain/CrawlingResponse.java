package com.leeryu.collect.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Builder
public class CrawlingResponse {
   private String title;
   private String url;
}
