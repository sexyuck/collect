package com.leeryu.collect.repository;

import com.leeryu.collect.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
