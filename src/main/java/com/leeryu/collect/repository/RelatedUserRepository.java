package com.leeryu.collect.repository;

import com.leeryu.collect.entity.RelatedUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RelatedUserRepository extends JpaRepository<RelatedUser, String> {
}
