package com.leeryu.collect.repository;

import com.leeryu.collect.entity.Site;
import com.leeryu.collect.entity.SiteId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SiteRepository extends JpaRepository<Site, SiteId> {

    List<Site> findAllBySiteIdEmail(final String email);
    void deleteBySiteIdEmailAndSiteIdUrl(final String email, final String url);
}
