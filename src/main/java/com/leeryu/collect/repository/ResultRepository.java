package com.leeryu.collect.repository;

import com.leeryu.collect.entity.Result;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ResultRepository extends JpaRepository<Result, Long> {
    List<Result> findAllByEmailOrderByCreateAtDesc(String email);
}
