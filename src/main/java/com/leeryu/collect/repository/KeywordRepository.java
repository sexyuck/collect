package com.leeryu.collect.repository;

import com.leeryu.collect.entity.Keyword;
import com.leeryu.collect.entity.KeywordId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KeywordRepository extends JpaRepository<Keyword, KeywordId> {

    List<Keyword> findAllByKeywordIdEmail(final String email);
    void deleteByKeywordIdEmailAndKeywordIdKeywordName(final String email, final String keywordName);
}
