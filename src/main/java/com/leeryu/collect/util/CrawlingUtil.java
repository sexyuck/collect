package com.leeryu.collect.util;

import com.leeryu.collect.domain.CrawlingResponse;
import com.leeryu.collect.enums.NewsSiteTypeCode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

@Slf4j
public class CrawlingUtil {
    public static List<CrawlingResponse> fetch(@NonNull String url, Function<Document, List<CrawlingResponse>> function) {
        Document document;
        try {
            document = Jsoup.connect(url).get();
            return function.apply(document);
        } catch (IOException ignored) {
            log.error("crawling error ::: {}", ignored.getMessage());
        }

        return Collections.emptyList();
    }

    public static void main(String[] args) {
        final NewsSiteTypeCode daum = NewsSiteTypeCode.DAUM;
        final List<CrawlingResponse> fetch = CrawlingUtil.fetch(daum.getUrl().replace("{query}", "우크라이나"), daum.getFunction());
        System.out.println(fetch);
    }
}
