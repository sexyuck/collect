package com.leeryu.collect.util;

public class ResponseResult {
    private ResponseResult() {
        throw new IllegalStateException("Utility class");
    }

    public static final String SUCCESS = "OK";
    public static final String FAIL = "FAIL";
}
