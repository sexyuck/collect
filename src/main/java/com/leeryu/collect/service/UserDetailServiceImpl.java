package com.leeryu.collect.service;

import com.leeryu.collect.entity.User;
import com.leeryu.collect.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserDetailServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userRepository.findById(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        return getAuthorityUser(user.getEmail(), user.getPassword());
    }

    @Override
    public org.springframework.security.core.userdetails.User save(String email, String password) {
        final boolean existsById = userRepository.existsById(email);
        if (existsById) {
            return null;
        }

        final User savedUser = userRepository.save(new User(email, passwordEncoder.encode(password)));
        return getAuthorityUser(savedUser.getEmail(), savedUser.getPassword());
    }

    private org.springframework.security.core.userdetails.User getAuthorityUser(String email, String password) {
        return new org.springframework.security.core.userdetails.User(email, password,
                Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")));
    }
}
