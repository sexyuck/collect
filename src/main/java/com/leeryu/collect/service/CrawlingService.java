package com.leeryu.collect.service;

import com.leeryu.collect.domain.CrawlingResponse;
import com.leeryu.collect.entity.Result;
import com.leeryu.collect.enums.NewsSiteTypeCode;
import com.leeryu.collect.repository.ResultRepository;
import com.leeryu.collect.util.CrawlingUtil;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class CrawlingService {

    private final ResultRepository repository;

    @Transactional
    public void crawling(String email, String keyword, @NonNull NewsSiteTypeCode newsSiteType) {
        final List<CrawlingResponse> fetchData = CrawlingUtil.fetch(newsSiteType.getUrl()
                .replace("{query}", keyword), newsSiteType.getFunction());

        Optional.ofNullable(fetchData)
                .ifPresent(data -> data
                        .forEach(e -> repository.save(
                                new Result(email, keyword, e.getTitle(), e.getUrl())
                        )));
    }
}
