package com.leeryu.collect.service;

import com.leeryu.collect.domain.response.SiteResponse;
import com.leeryu.collect.entity.RelatedKeyword;
import com.leeryu.collect.entity.RelatedUser;
import com.leeryu.collect.entity.Site;
import com.leeryu.collect.enums.NewsSiteTypeCode;
import com.leeryu.collect.repository.RelatedUserRepository;
import com.leeryu.collect.repository.SiteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class SiteService {

    private final SiteRepository siteRepository;
    private final RelatedUserRepository relatedUserRepository;
    private final CrawlingService crawlingService;

    public List<SiteResponse> getSitesByLoginUser(String email) {
        return siteRepository.findAllBySiteIdEmail(email)
                .stream().map(SiteResponse::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public void register(String email, String site) {
        siteRepository.save(Site.createSite(email, site));

        final RelatedUser relatedUser = relatedUserRepository.findById(email)
                .orElseThrow(() -> new UsernameNotFoundException("user not found"));

        final List<RelatedKeyword> keywords = relatedUser.getKeywords();
        for (RelatedKeyword keyword : keywords) {
            crawlingService.crawling(email, keyword.getKeywordId().getKeywordName(), NewsSiteTypeCode.findByName(site));
        }
    }

    @Transactional
    public void delete(String email, String site) {
        siteRepository.deleteBySiteIdEmailAndSiteIdUrl(email, site);
    }
}
