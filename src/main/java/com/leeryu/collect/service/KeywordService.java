package com.leeryu.collect.service;

import com.leeryu.collect.domain.response.KeywordResponse;
import com.leeryu.collect.entity.Keyword;
import com.leeryu.collect.repository.KeywordRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Slf4j
@RequiredArgsConstructor
@Service
public class KeywordService {

    private final KeywordRepository keywordRepository;

    public List<KeywordResponse> getKeywordByLoginUser(String email) {
        return keywordRepository.findAllByKeywordIdEmail(email)
                .stream().map(KeywordResponse::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public void register(String email, String keyword) {
        keywordRepository.save(Keyword.createKeyword(email, keyword));
    }

    @Transactional
    public void delete(String email, String keyword) {
        keywordRepository.deleteByKeywordIdEmailAndKeywordIdKeywordName(email, keyword);
    }
}
